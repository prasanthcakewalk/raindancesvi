# Welcome to RainDancesVI!

RainDancesVI is TensorFlow-based implementation of Independent Classifier Networks (InClass nets) in Python 3. InClass nets can be used for

* Nonparametric estimatimation of conditional independence mixture models
* Unsupervised classification

Project website: <https://prasanthcakewalk.gitlab.io/raindancesvi/>  

### References and citation guide

If you use the techniques implemented in RainDancesVI in your work, please consider citing the original paper that introduced them.

* Konstantin T. Matchev, Prasanth Shyamsundar, _"InClass Nets: Independent Classifier Networks for Nonparametric Estimation of Conditional Independence Mixture Models and Unsupervised Classification"_, [arXiv:2009.00131 [stat.ML]](https://arxiv.org/abs/2009.00131).

### Copyright

Copyright &copy; 2020 Konstantin T. Matchev and Prasanth Shyamsundar  
<details>
<summary>RainDancesVI is licensed under the MIT License (click to expand).</summary>
<br>
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
</details>
